package com.epam;

import com.epam.driverprovider.DriverProvider;
import com.epam.model.User;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.List;

import static com.epam.utils.ConfigProperties.getCredentials;
import static com.epam.utils.Constants.*;

public abstract class BaseTest {
    protected List<User> users;

    @BeforeMethod
    public void setUp() {
        DriverProvider.getDriver().get(BASE_URL);
        users = getCredentials();
    }

    @AfterMethod
    public void cleanUp() {
        DriverProvider.quitDriver();
    }
}
