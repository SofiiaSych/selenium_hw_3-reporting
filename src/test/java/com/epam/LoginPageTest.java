package com.epam;

import com.epam.bo.IncomeLettersBO;
import com.epam.bo.LoginBO;
import com.epam.bo.MessageBO;
import com.epam.bo.OutcomeLettersBO;
import com.epam.listener.CustomListener;
import com.epam.model.User;
import com.paypal.selion.platform.dataprovider.DataProviderFactory;
import com.paypal.selion.platform.dataprovider.DataResource;
import com.paypal.selion.platform.dataprovider.SeLionDataProvider;
import com.paypal.selion.platform.dataprovider.impl.InputStreamResource;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;

import static com.epam.driverprovider.DriverProvider.DRIVER_POOL;
import static com.epam.utils.Constants.*;

//@Listeners(CustomListener.class)
public class LoginPageTest extends BaseTest {

    protected IncomeLettersBO incomeLettersBO;
    protected LoginBO loginPageBO;
    protected OutcomeLettersBO outcomeLettersBO;
    protected MessageBO messageBO;

    @DataProvider(name = "userCredentials", parallel = true)
    public Object[][] dataProvider() throws IOException {
        DataResource resource =
                new InputStreamResource(new FileInputStream(USER_DATA),
                        User.class, "json");
        SeLionDataProvider dataProvider =
                DataProviderFactory.getDataProvider(resource);
        return dataProvider.getAllData();
    }

    @Test(dataProvider = "userCredentials")
    @Severity(SeverityLevel.NORMAL)
    @Description("Verify sending and deleting the last letter")
    @Step("Verify sending and deleting the last letter")
    public void verifyMessageIsSentAndDelete(User user) {

        incomeLettersBO = new IncomeLettersBO(DRIVER_POOL.get());
        loginPageBO = new LoginBO(DRIVER_POOL.get());
        outcomeLettersBO = new OutcomeLettersBO(DRIVER_POOL.get());
        messageBO = new MessageBO(DRIVER_POOL.get());

        loginPageBO.typeLoginPasswordAndSubmit(user.getEmail(), user.getPassword());
        String userEmail = incomeLettersBO.getUserEmail();
        Assert.assertEquals(user.getEmail(), userEmail, "Incorrect user email");

        incomeLettersBO.openMessageWindow();
        Assert.assertTrue(messageBO.checkSubjectFieldIsDisplayed(), "Message window doesn't appear");

        messageBO.fillMessage(RECEIVER, SUBJECT, MESSAGE);

        messageBO.sendMessage();
        Assert.assertTrue(messageBO.checkPopUpIsDisplayed(), "PopUp doesn't appear");

        outcomeLettersBO.openOutcomeLetters();
        Assert.assertEquals(outcomeLettersBO.getMessageTitleNameText(), SUBJECT, "Receiver subject isn't as expected");

        incomeLettersBO.openBurgerMenu();

        outcomeLettersBO.selectLastMessage();
        Assert.assertTrue(outcomeLettersBO.checkLastMessageCheckBoxIsSelected(), "The letter isn't selected");
        outcomeLettersBO.deleteLastMessage();
    }
}
