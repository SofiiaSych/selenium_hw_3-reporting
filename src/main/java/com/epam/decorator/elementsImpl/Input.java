package com.epam.decorator.elementsImpl;

import org.openqa.selenium.WebElement;

import com.epam.decorator.AbstractElement;

public class Input extends AbstractElement {
    public Input(WebElement element) {
        super(element);
    }

    public void type(String input) {
        element.clear();
        element.sendKeys(input);
    }

}
