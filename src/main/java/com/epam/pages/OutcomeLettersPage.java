package com.epam.pages;


import com.epam.decorator.elementsImpl.Button;
import com.epam.decorator.elementsImpl.CheckBox;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OutcomeLettersPage extends BasePage {

    public OutcomeLettersPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@class='yW']//span")
    private WebElement letterReceiverText;

    @FindBy(css = "div.TK > div:nth-child(4) > div")
    private Button outcomeLettersBth;

    @FindBy(xpath = "//tr[@class = 'zA yO'][1]//descendant::*[@class='bog']/span")
    private WebElement messageTitleName;

    @FindBy(xpath = "(//tr[@jscontroller]//div[@role='checkbox'])[4]")
    private CheckBox selectLastMessageCheckBox;

    @FindBy(xpath = "(//*[@act=10 and @role='button'])[2]")
    private Button deleteLastMessageBtn;

    public WebElement getLetterReceiverText() {
        return letterReceiverText;
    }

    public Button getOutcomeLettersBth() {
        return outcomeLettersBth;
    }

    public Button getDeleteLastMessageBtn() {
        return deleteLastMessageBtn;
    }

    public WebElement getMessageTitleName() {
        return messageTitleName;
    }

    public CheckBox getSelectLastMessageCheckBox() {
        return selectLastMessageCheckBox;
    }
}
