package com.epam.pages;

import com.epam.decorator.elementsImpl.Input;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;


public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@type='email']")
    private Input loginField;

    @FindBy(xpath = "//input[@name = 'password']")
    private Input passwdField;

    public Input getLoginField() {
        return loginField;
    }

    public Input getPasswdField() {
        return passwdField;
    }

}
