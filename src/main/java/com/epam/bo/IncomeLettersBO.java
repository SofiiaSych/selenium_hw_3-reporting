package com.epam.bo;

import com.epam.pages.IncomeLettersPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IncomeLettersBO {

    private IncomeLettersPage incomeLettersPage;
    private static final Logger log = LogManager.getLogger(IncomeLettersBO.class);

    public IncomeLettersBO(WebDriver driver) {
        incomeLettersPage = new IncomeLettersPage(driver);
    }

    public String getUserEmail() {
        log.info("getting user email");
        incomeLettersPage.getUserImg().click();
        new WebDriverWait(incomeLettersPage.driver, 10).until(ExpectedConditions.visibilityOf
                (incomeLettersPage.getUserImg()));
        String email = incomeLettersPage.getEmail().getText();
        return email;
    }

    public void openMessageWindow() {
        log.info("Opening message window");
        incomeLettersPage.getComposeBtn().waitUntilVisibleAndClick();
    }

    public void openBurgerMenu() {
        log.info("opening burger menu");
        incomeLettersPage.getBurgerMenuBtn().waitUntilVisibleAndClick();
    }
}
