package com.epam.bo;

import com.epam.pages.OutcomeLettersPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class OutcomeLettersBO {
    private OutcomeLettersPage outcomeLettersPage;
    private static final Logger log = LogManager.getLogger(OutcomeLettersBO.class);

    public OutcomeLettersBO(WebDriver driver) {
        outcomeLettersPage = new OutcomeLettersPage(driver);
    }

    public void openOutcomeLetters() {
        log.info("opening outcome letters");
        outcomeLettersPage.getOutcomeLettersBth().waitUntilVisibleAndClick();
    }

    public void deleteLastMessage() {
        log.info("deleting last message");
        outcomeLettersPage.getDeleteLastMessageBtn().click();
    }

    public void selectLastMessage() {
        log.info("selecting last message");
        outcomeLettersPage.getSelectLastMessageCheckBox().setChecked(true);
    }

    public String getMessageTitleNameText() {
        log.info("getting message title name");
        return outcomeLettersPage.getMessageTitleName().getText();
    }

    public boolean checkLastMessageCheckBoxIsSelected() {
        log.info("checking if last message is selected");
        return outcomeLettersPage.getSelectLastMessageCheckBox().isSelected();
    }
}
